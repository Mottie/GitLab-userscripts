# GitLab userscripts

Userscripts to add functionality to GitLab.

## Installation

1. Make sure you have user scripts enabled in your browser (these instructions refer to the latest versions of the browser):
  * Firefox - install [Greasemonkey](https://addons.mozilla.org/en-US/firefox/addon/greasemonkey/).
  * Chrome - install [Tampermonkey](https://tampermonkey.net/?ext=dhdg&browser=chrome) or [NinjaKit](https://chrome.google.com/webstore/detail/gpbepnljaakggeobkclonlkhbdgccfek).
  * Opera - install [Tampermonkey](https://tampermonkey.net/?ext=dhdg&browser=opera) or [Violent Monkey](https://addons.opera.com/en/extensions/details/violent-monkey/).
  * Safari - install [Tampermonkey](https://tampermonkey.net/?ext=dhdg&browser=safari) or [NinjaKit](http://ss-o.net/safari/extension/NinjaKit.safariextz).
  * Dolphin - install [Tampermonkey](https://tampermonkey.net/?ext=dhdg&browser=dolphin).
  * UC Browser - install [Tampermonkey](https://tampermonkey.net/?ext=dhdg&browser=ucweb).

2. Get information or install:
  * Learn more about the userscript by clicking on the named link. You will be taken to the specific wiki page.
  * Install a script directly from GitHub by clicking on the "install" link in the table below.
  * Install a script from [GreasyFork](https://greasyfork.org/en/users/24847-mottie) (GF) from the userscript site page
  * Or, install the scripts from [OpenUserJS](https://openuserjs.org/users/Mottie/scripts) (OU).<p></p>

| Userscript Wiki                        | ![][ico] | Direct<br>Install | Sites                 | Created    | Updated    |
|----------------------------------------|:---:|:------------------:|:-------------------------:|:----------:|:----------:|
| [GitLab collapse in comment][cic-wiki] |     | [install][cic-raw] | [GF][cic-gf] [OU][cic-ou] | 2017.05.24 | 2017.05.24 |
| [GitLab collapse markdown][cmd-wiki]   |     | [install][cmd-raw] | [GF][cmd-gf] [OU][cmd-ou] | 2017.05.24 | 2017.05.24 |
| [GitLab sort content][gsc-wiki]        |     | [install][gsc-raw] | [GF][gsc-gf] [OU][gsc-ou] | 2018.04.07 | 2018.04.09 |

\* The ![][ico] column indicates that the userscript has been included in the [Octopatcher](https://github.com/Mottie/Octopatcher) browser extension/addon (coming soon).

[cic-wiki]: https://gitlab.com/Mottie/GitLab-userscripts/wikis/GitLab-collapse-in-comment
[cmd-wiki]: https://gitlab.com/Mottie/GitLab-userscripts/wikis/GitLab-collapse-markdown
[gsc-wiki]: https://gitlab.com/Mottie/GitLab-userscripts/wikis/GitLab-sort-content

[cic-raw]: https://gitlab.com/Mottie/GitLab-userscripts/raw/master/gitlab-collapse-in-comment.user.js
[cmd-raw]: https://gitlab.com/Mottie/GitLab-userscripts/raw/master/gitlab-collapse-markdown.user.js
[gsc-raw]: https://gitlab.com/Mottie/GitLab-userscripts/raw/master/gitlab-collapse-markdown.user.js

[cic-gf]: https://greasyfork.org/en/scripts/30007-gitlab-collapse-in-comment
[cmd-gf]: https://greasyfork.org/en/scripts/30006-gitlab-collapse-markdown
[gsc-gf]: https://greasyfork.org/en/scripts/40406-gitlab-sort-content

[cic-ou]: https://openuserjs.org/scripts/Mottie/GitLab_Collapse_In_Comment
[cmd-ou]: https://openuserjs.org/scripts/Mottie/GitLab_Collapse_Markdown
[gsc-ou]: https://openuserjs.org/scripts/Mottie/GitLab_Sort_Content

[ico]: https://raw.githubusercontent.com/Mottie/Octopatcher/master/src/images/icon16.png

## Updating

Userscripts are set up to automatically update. You can check for updates from within the Greasemonkey or Tampermonkey menu, or click on the install link again to get the update.

Each individual userscript's change log is contained on its individual wiki page.

## Issues

Please report any userscript issues within this repository's [issue section](https://gitlab.com/Mottie/GitLab-userscripts/issues). Greasyfork messages are also received, but not as easily tracked. Thanks!
